<?php include('inc/pdo.php');
include('inc/functions.php');

$error = array();

if (isset($_POST['submitted'])) {

    // Protection faille XSS
    $email      = trim(strip_tags($_POST['email']));
    $password   = trim(strip_tags($_POST['password']));

//Requete sur identité utilisateur (s'il existe)
  $sql   = "SELECT *
            FROM nt_user
            WHERE email = :email";
  $query = $pdo -> prepare($sql);
  $query -> bindValue(':email', $email, PDO::PARAM_STR);
  $query -> execute();
  $user = $query -> fetch();

  if (!empty($user)) {
     if ($password !== $user['password']){
       $error['password'] = 'Mot de passe erroné';
     }
  } else {
    $error['email'] = 'Accès refusé';
  }

    if (count($error) == 0) {

      $_SESSION['nt_user'] = array(
        'id'      => $user['id'],
        'email'   => $user['email'],
        'role'    => $user['role'],
        'ip'      => $_SERVER['REMOTE_ADDR']
      );
      if (isAdmin()) {
        header('Location: index.php');
      } else {
        header('Location: 404.php');
      }
    }
}

$title = "Network-Tracker - Se connecter";
?>


<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title><?= $title; ?></title>
  <!-- Favicon -->
  <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
  <link href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="assets/css/argon.css?v=1.0.0" rel="stylesheet">
</head>

<body class="bg-default">
  <div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark">
      <div class="container">
        <div class="collapse navbar-collapse" id="navbar-collapse-main">
          <!-- Collapse header -->
          <div class="navbar-collapse-header d-md-none">
            <div class="row">
              <div class="col-6 collapse-brand">
                <a href="index.php">
                    <img src="https://cdn.discordapp.com/attachments/534651155519897611/534651183206236171/logo.png">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary py-7 py-lg-8">
      <div class="container">
        <div class="header-body text-center mb-7">
          <div class="row justify-content-center">
            <div class="col-lg-5 col-md-6">
              <h1 class="text-white">Bienvenue sur Network Tracker !</h1>
              <p class="text-lead text-light">Vous devez vous connecter pour accéder aux fonctionnalités de notre application.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
      </div>
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
          <div class="card bg-secondary shadow border-0">
            <div class="card-body px-lg-5 py-lg-5">
              <div class="text-center text-muted mb-4">
                <small>Connectez-vous ci-dessous</small>
              </div>
              <form role="form" method="post" action="">
                <div class="form-group mb-3">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input class="form-control" name="email" placeholder="Email" type="email" value="<?php if(!empty($_POST['email'])) {echo $_POST['email']; } ?>">
                      <?php if(!empty($error['email'])) { echo $error['email']; } ?>
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                    </div>
                    <input class="form-control" name="password" placeholder="Mot de passe" type="password">
                      <?php if(!empty($error['password'])) { echo $error['password']; } ?>
                  </div>
                </div>
                <div class="text-center">
                  <input type="submit" class="btn btn-primary my-4" name="submitted" value="Se connecter">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <footer class="py-5">
    <div class="container">
      <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-12">
          <div class="copyright text-center text-xl-center text-muted">
            &copy; 2019 Network Tracker
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <!-- Argon JS -->
  <script src="assets/js/argon.js?v=1.0.0"></script>
</body>

</html>