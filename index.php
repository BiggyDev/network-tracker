<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>


<script type="text/javascript" src="jspdf.min.js"></script>
<script type="text/javascript" src="html2canvas.js"></script>

<script>
    function genPDF() {

        doc.setFontSize(40);
        doc.text(35, 25, 'Paranyan loves jsPDF');
        doc.addImage(imgData, 'JPEG', 15, 40, 180, 160)
    }
</script>

<?php
include('inc/pdo.php');
include('inc/functions.php');
include('function.php');

$error = array();
$success = false;

$sql= "SELECT id, name, ip, mask, macaddr FROM nt_server";
$query = $pdo-> prepare($sql);
$query -> execute();
$serveurs = $query -> fetchAll();

$json_source = file_get_contents('upload/trame.json');
$json_data = json_decode($json_source, true);
$nbtotaltrame = countnbtrametotal($json_data);


$toto = 0;
$protocols = array();

$ipsourcesautho = array();
$ipdestinationsauto = array();
$ipsourcesforbiden = array();
$ipdestinationsforbiden = array();


foreach ($serveurs as $server){
    $srvcount = array(
            "name" => $server['name'],
            "authorize"=>0,
            "forbidden"=>0,
            $ipsourcesautho,
            $ipdestinationsauto,
            $ipsourcesforbiden,
            $ipdestinationsforbiden
    );

    foreach($json_data as $trame) {

        if (isset($trame['_source']['layers']['ip']['ip.src']) && isset($trame['_source']['layers']['ip']['ip.dst']) ){
            if(cidr_match($trame['_source']['layers']['ip']['ip.src'], $server['ip'] . '/' . $server['mask']) == true) {
                if(cidr_match($trame['_source']['layers']['ip']['ip.dst'], $server['ip'] . '/' . $server['mask']) == true){
                    $srvcount['authorize']++;
                }
            }
            else{
                $srvcount['forbidden']++;

                if (!in_array($trame['_source']['layers']['ip']['ip.src'],$srvcount[2])){
                    $srvcount[2][]=$trame['_source']['layers']['ip']['ip.src'];
                }

                if (!in_array($trame['_source']['layers']['ip']['ip.dst'],$srvcount[3])){
                    $srvcount[3][]=$trame['_source']['layers']['ip']['ip.dst'];
                }
                $toto++;
            }
        }
        $protocols = countprotocol($trame,$protocols);
    }
    $totaltable[]= $srvcount;
}



foreach ($json_data as $trame){
    $protocols = countprotocol($trame,$protocols);
}

if(isset($_POST['sendmail'])){
    require_once('tcpdf/tcpdf.php');
    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $obj_pdf->AddPage();
    $obj_pdf->SetTitle("Rapport de l'analyse de votre fichier");
    $obj_pdf->SetHeaderData('','',PDF_HEADER_TITLE,PDF_HEADER_STRING);
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN,'',PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA,'',PDF_FONT_SIZE_DATA));
    $obj_pdf->setdefaultmonospacedFont('helvetica');
    $obj_pdf->setFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->setmargins(PDF_MARGIN_LEFT,'5',PDF_MARGIN_RIGHT);
    $obj_pdf->SetPrintHeader(false);
    $obj_pdf->SetPrintFooter(false);
    $obj_pdf->setautopagebreak(TRUE,10);
    $obj_pdf->setFont('helvetica','',12);

    $content = '<br><br>
                <style>
                
                table {
                  border-collapse: collapse;
                }
                
                table, th, td {
                  border: 1px solid black;
                }
                
                h1{
                    text-align: center;
                }
                
                
                </style>';

    if (isset($serveurs)){
        $content .= '<h1> Tableau du/des sous-réseau(x)</h1>
                        <table>
                             <tr>
                                 <th>Nom du réseau</th>
                                 <th>Adresse IP</th>
                                 <th>Masque</th>
                                 <th>Adresse MAC</th>
                             </tr>';

        foreach ($serveurs as $serveur) {
            $content .= '<tr>';
            $content .= '<td>' . $serveur['name'] . '</td>';
            $content .= '<td>' . $serveur['ip'] . '</td>';
            $content .= '<td>' . $serveur['mask'] . '</td>';
            $content .= '<td>' . $serveur['macaddr'] . '</td>';
            $content .= '</tr>';
        }

    }

    $content .= '</table><br><br><h1> Résumé de l\'analyse</h1><br>';
    $content .= 'Nombre de protocoles différents: ' . count($protocols) . '<br>';
    $content .= 'Nombre de trames: ' . $nbtotaltrame . '<br><br>';

    if (isset($totaltable)){
        $content .= '<table>
                        <tr>
                            <th>Sous réseaux</th>
                            <th>NOMBRE DE REQUÊTES AUTORISÉES</th>
                            <th>NOMBRE DE REQUÊTES INTERDITES</th>
                        </tr>';

        foreach ($totaltable as $table){
            $content .= '<tr>';
            $content .= '<td>'. $table['name'] . '</td>';
            $content .= '<td>'. $table['authorize'] . '</td>';
            $content .= '<td>'. $table['forbidden'] . '</td>';
            $content .= '</tr>';
        }
        $content .= '</table>';
    }
    $content .= 'Rapport créé le: ' . date("d M Y, à G:i", time()) . '.';

    $obj_pdf->writeHTML($content);
    ob_end_clean();
    $obj_pdf->Output(dirname(__FILE__).'/rapport.pdf', 'F');

    //Envoi du mail
    header('Location: mail.php');

}


if (isset($_POST['create_pdf'])){
    require_once('tcpdf/tcpdf.php');
    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $obj_pdf->AddPage();
    $obj_pdf->SetTitle("Rapport de l'analyse de votre fichier");
    $obj_pdf->SetHeaderData('','',PDF_HEADER_TITLE,PDF_HEADER_STRING);
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN,'',PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA,'',PDF_FONT_SIZE_DATA));
    $obj_pdf->setdefaultmonospacedFont('helvetica');
    $obj_pdf->setFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->setmargins(PDF_MARGIN_LEFT,'5',PDF_MARGIN_RIGHT);
    $obj_pdf->SetPrintHeader(false);
    $obj_pdf->SetPrintFooter(false);
    $obj_pdf->setautopagebreak(TRUE,10);
    $obj_pdf->setFont('helvetica','',12);

    $content = '<br><br>
                <style>
                
                table {
                  border-collapse: collapse;
                }
                
                table, th, td {
                  border: 1px solid black;
                }
                
                h1{
                    text-align: center;
                }
                
                
                </style>';

    if (isset($serveurs)){
        $content .= '<h1> Tableau du/des sous-réseau(x)</h1>
                        <table>
                             <tr>
                                 <th>Nom du réseau</th>
                                 <th>Adresse IP</th>
                                 <th>Masque</th>
                                 <th>Adresse MAC</th>
                             </tr>';

        foreach ($serveurs as $serveur) {
            $content .= '<tr>';
            $content .= '<td>' . $serveur['name'] . '</td>';
            $content .= '<td>' . $serveur['ip'] . '</td>';
            $content .= '<td>' . $serveur['mask'] . '</td>';
            $content .= '<td>' . $serveur['macaddr'] . '</td>';
            $content .= '</tr>';
        }

    }

    $content .= '</table><br><br><h1> Résumé de l\'analyse</h1><br>';
    $content .= 'Nombre de protocoles différents: ' . count($protocols) . '<br>';
    $content .= 'Nombre de trames: ' . $nbtotaltrame . '<br><br>';

    if (isset($totaltable)){
        $content .= '<table>
                        <tr>
                            <th>Sous réseaux</th>
                            <th>NOMBRE DE REQUÊTES AUTORISÉES</th>
                            <th>NOMBRE DE REQUÊTES INTERDITES</th>
                        </tr>';

        foreach ($totaltable as $table){
            $content .= '<tr>';
            $content .= '<td>'. $table['name'] . '</td>';
            $content .= '<td>'. $table['authorize'] . '</td>';
            $content .= '<td>'. $table['forbidden'] . '</td>';
            $content .= '</tr>';
        }
        $content .= '</table>';
    }
    $content .= 'Rapport créé le: ' . date("d M Y, à G:i", time()) . '.';

    $obj_pdf->writeHTML($content);
    ob_end_clean();
    $obj_pdf->Output("test.pdf","D");
}

if (isset($_POST['submitted'])) {

    // Protection faille XSS
    $macaddr        = trim(strip_tags($_POST['macaddr']));
    $ip             = trim(strip_tags($_POST['ip']));
    $mask           = trim(strip_tags($_POST['mask']));
    $name           = trim(strip_tags($_POST['name']));

    if(isset($ip)){
        if (!filter_var($ip, FILTER_VALIDATE_IP)) {
            $error['ip'] = 'L\'adresse IP ' . $ip . ' est considérée comme invalide';
        }
    }
    if(isset($name)){
        if(strlen($name) < 5 ) {
            $error['name'] = 'Champ trop court. (Minimum ' . 5 . ' caractères)';
        } elseif(strlen($name) > 100) {
            $error['name'] = 'Champ trop long. (Maximum ' . 100 . ' caractères)';
        }
    } else {
        $error['name'] = 'Veuillez renseigner ce champ';
    }

    if(isset($mask)){
        if(8 > $mask || $mask > 25) {
            $error['mask'] = 'Veuillez sélectionnez un masque de sous-réseau';
        }
    } else {
        $error['mask'] = 'Veuillez renseigner ce champ';
    }

    // Si aucune error
        if (count($error) == 0) {
            $success = true;
            $sql = "INSERT INTO nt_server (macaddr,ip,mask,name)
                VALUES (:macaddr, :ip, :mask, :name)";
            // preparation de la requête
            $query = $pdo->prepare($sql);
            // Protection injections SQL
            $query->bindValue(':macaddr', $macaddr, PDO::PARAM_STR);
            $query->bindValue(':ip', $ip, PDO::PARAM_STR);
            $query->bindValue(':mask', $mask, PDO::PARAM_STR);
            $query->bindValue(':name', $name, PDO::PARAM_STR);
            // execution de la requête preparé
            $query->execute();
            header('location:index.php');
        }
    }

if (isset($_POST['m_submitted']) && isset($serveur['id'])) {

    $id = $serveur['id'];

    // Protection faille XSS
    $m_macaddr = trim(strip_tags($_POST['m_macaddr']));
    $m_ip = trim(strip_tags($_POST['m_ip']));
    $m_mask = trim(strip_tags($_POST['m_mask']));
    $m_name = trim(strip_tags($_POST['m_name']));

    if (isset($m_ip)) {
        if (!filter_var($m_ip, FILTER_VALIDATE_IP)) {
            $error['m_ip'] = 'L\'adresse IP ' . $m_ip . ' est considérée comme invalide';
        }
    }
    if (isset($m_name)) {
        if (strlen($m_name) < 5) {
            $error['m_name'] = 'Champ trop court. (Minimum ' . 5 . ' caractères)';
        } elseif (strlen($m_name) > 100) {
            $error['m_name'] = 'Champ trop long. (Maximum ' . 100 . ' caractères)';
        }
    } else {
        $error['m_name'] = 'Veuillez renseigner ce champ';
    }

    if (isset($m_mask)) {
        if (8 > $m_mask || $m_mask > 25) {
            $error['m_mask'] = 'Veuillez sélectionnez un masque de sous-réseau';
        }
    } else {
        $error['m_mask'] = 'Veuillez renseigner ce champ';
    }

    // Si aucune error
    if (count($error) == 0) {
        $success = true;
        $sql = "UPDATE nt_server SET macaddr = :macaddr, ip = :ip, mask = :mask, name = :name WHERE id = $id";
        $query = $pdo->prepare($sql);
        $query->bindValue(':id', $id, PDO::PARAM_INT);
        $query->bindValue(':macaddr', $m_macaddr, PDO::PARAM_STR);
        $query->bindValue(':ip', $m_ip, PDO::PARAM_STR);
        $query->bindValue(':mask', $m_mask, PDO::PARAM_STR);
        $query->bindValue(':name', $m_name, PDO::PARAM_STR);
        $query->execute();
//        header('location:index.php');
    }
}

if (isLogged() && isAdmin()) { ?>

<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>Network Tracker</title>
    <!-- Favicon -->
    <link href="./assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="./assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
    <link href="./assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <!-- Argon CSS -->
    <link type="text/css" href="./assets/css/argon.css?v=1.0.0" rel="stylesheet">
</head>

<body>
<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
        <div class="container-fluid" style="position: absolute">
            <!-- Disconnect -->
            <a href="disconnect.php" style="position: relative; top: 10px; left: 5px;"><i class="ni ni-2x ni-button-power text-white"></i></a>
        </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->
                <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h3 class="card-title text-uppercase mb-0">Ajouter un serveur</h3><br>
                                        <span class="text-warning">
                                            <?php
                                            if (isset($_POST['submitted'])) {
                                                if ($success == true)
                                                    echo 'Ajout du serveur effectué avec succès';
                                                else
                                                    echo 'Erreur(s) rencontrée(s) dans un/plusieurs champ(s)';

                                            } ?>
                                        </span>
                                    </div>
                                    <div class="col-auto">
                                        <a class="btn btn-primary" title="Cliquez pour afficher le formulaire" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            <i class="ni ni-fat-add"></i>
                                        </a>
                                    </div>
                                    <div class="collapse" id="collapseExample">
                                        <div class="card card-body">
                                            <form method="post" action="" id="addserver">
                                                <div class="card-group">
                                                    <div class="form-group col-lg-12">
                                                        <div class="input-group input-group-alternative">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><i class="ni ni-bold-right"></i></span>
                                                            </div>
                                                            <input class="form-control" id="macaddr" name="macaddr" placeholder="Adresse MAC" type="text" value="<?php if(isset($_POST['macaddr'])) {echo $_POST['macaddr']; } ?>">
                                                        </div>
                                                        <?php if(isset($error['macaddr'])) { echo $error['macaddr']; } ?>
                                                    </div>
                                                    <div class="form-group col-lg-12">
                                                        <div class="input-group input-group-alternative">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><i class="ni ni-bold-right"></i></span>
                                                            </div>
                                                            <input class="form-control" id="ip" name="ip" placeholder="Adresse IP" type="text" value="<?php if(isset($_POST['ip'])) {echo $_POST['ip']; } ?>">
                                                        </div>
                                                        <?php if(isset($error['ip'])) { echo $error['ip']; } ?>
                                                    </div>
                                                    <div class="form-group col-lg-12">
                                                        <div class="input-group input-group-alternative">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><i class="ni ni-bold-right"></i></span>
                                                            </div>
                                                            <select class="form-control" id="mask" name="mask">
                                                                <option name="mask" selected="selected">Masque de sous-réseau</option>
                                                                <?php for ($i = 25; $i >= 8; $i--)
                                                                { ?>
                                                                    <option name="mask" value="<?= $i; ?>"><?= "/" . $i; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <?php if(isset($error['mask'])) { echo $error['mask']; } ?>
                                                    </div>
                                                    <div class="form-group col-lg-12">
                                                        <div class="input-group input-group-alternative">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><i class="ni ni-bold-right"></i></span>
                                                            </div>
                                                            <input class="form-control" id="name" name="name" placeholder="Nom du réseau" type="text" value="<?php if(isset($_POST['name'])) {echo $_POST['name']; } ?>">
                                                        </div>
                                                        <?php if(isset($error['name'])) { echo $error['name']; } ?>
                                                    </div>
                                                    <div class="text-center">
                                                        <input type="submit" class="btn btn-primary my-4" name="submitted" value="Ajouter">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Nombre de serveurs</h5>
                                        <span class="h2 font-weight-bold mb-0"><?= count($serveurs); ?></span>
                                    </div>
                                    <div class="col-auto">
                                        <a class="btn btn-primary" title="Cliquez pour afficher le tableau" data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            <i class="ni ni-bold-down"></i>
                                        </a>
                                    </div>
                                    <div class="collapse" id="collapseExample2">
                                        <div class="card card-body">
                                            <table class="table align-items-center table-flush">
                                                <tr>
                                                    <th>Nom du réseau</th>
                                                    <th>Adresse IP</th>
                                                    <th>Masque</th>
                                                    <th>Adresse MAC</th>
                                                    <th>Actions</th>
                                                </tr>
                                                    <?php foreach ($serveurs as $serveur) { ?>
                                                <tr>
                                                    <td><?= $serveur['name']; ?></td>
                                                    <td><?= $serveur['ip']; ?></td>
                                                    <td><?= $serveur['mask']; ?></td>
                                                    <td><?= $serveur['macaddr']; ?></td>
                                                    <td>
                                                        <!-- Modifier -->
                                                        <button class="ni ni-settings" data-toggle="modal" data-target="#exampleModal2"></button>
                                                        <br>
                                                        <!-- Supprimer -->
                                                        <button class="ni ni-fat-remove" data-toggle="modal" data-target="#exampleModal"></button>

                                                        <!-- Modal Modify -->
                                                        <div class="modal" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Modifiez les champs que vous souhaitez :</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form method="post" action="">
                                                                        <div class="modal-body">
                                                                            <div class="input-group input-group-alternative">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text"><i class="ni ni-bold-right"></i></span>
                                                                                </div>
                                                                                <input class="form-control" id="macaddr" name="m_macaddr" placeholder="Adresse MAC" type="text" value="<?php if(isset($serveur['macaddr'])){ echo $serveur['macaddr'];} ?>">
                                                                            </div>
                                                                            <?php if(isset($error['m_macaddr'])) { echo $error['m_macaddr']; } ?>
                                                                            <div class="input-group input-group-alternative">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text"><i class="ni ni-bold-right"></i></span>
                                                                                </div>
                                                                                <input class="form-control" id="ip" name="m_ip" placeholder="Adresse IP" type="text" value="<?php if(isset($serveur['ip'])){ echo $serveur['ip'];} ?>">
                                                                            </div>
                                                                            <?php if(isset($error['m_ip'])) { echo $error['m_ip']; } ?>
                                                                            <div class="input-group input-group-alternative">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text"><i class="ni ni-bold-right"></i></span>
                                                                                </div>
                                                                                <select class="form-control" id="mask" name="m_mask">
                                                                                    <option name="m_mask" selected="selected">Masque de sous-réseau</option>
                                                                                    <?php for ($i = 25; $i >= 8; $i--) { ?>
                                                                                        <option name="m_mask" value="<?= $i; ?>"><?= "/" . $i; ?></option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </div>
                                                                            <?php if(isset($error['m_mask'])) { echo $error['m_mask']; } ?>
                                                                            <div class="input-group input-group-alternative">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text"><i class="ni ni-bold-right"></i></span>
                                                                                </div>
                                                                                <input class="form-control" id="name" name="m_name" placeholder="Nom du réseau" type="text" value="<?php if(isset($serveur['name'])){ echo $serveur['name'];} ?>">
                                                                            </div>
                                                                            <?php if(isset($error['m_name'])) { echo $error['m_name']; } ?>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                                                            <input type="submit" class="btn btn-primary" name="m_submitted" value="Modifier">
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Modal Delete-->
                                                        <div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Êtes vous sûr ?</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        Êtes vous sûr de vouloir supprimer ce réseau ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                                                        <a href="deleteserver.php?id=<?= $serveur['id']; ?>"><button class="btn btn-primary">Supprimer</button></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if(isset($totaltable)){?>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Nombre de protocoles</h5>
                                        <span class="h2 font-weight-bold mb-0"><?= count($protocols); ?></span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                                            <i class="fas fa-chart-pie"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Nombre de trames analysés</h5>
                                        <span class="h2 font-weight-bold mb-0"><?php echo $nbtotaltrame * count($serveurs); ?></span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                            <i class="fas fa-chart-bar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h3 class="card-title text-uppercase mb-0">M'envoyer un rapport par mail</h3>
                                    </div>

                                    <div class="col-auto">
                                        <form method="post">
                                            <input type="submit" name="sendmail" class="btn btn-primary" title="Cliquez pour envoyer le rapport par mail" data-toggle="tooltip" role="button" value="Envoyer">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h3 class="card-title text-uppercase mb-0">Générer un rapport PDF</h3>
                                    </div>
                                    <div class="col-auto">
                                        <form method="post">
                                            <input type="submit" name="create_pdf" class="btn btn-primary " title="Cliquez pour télécharger un compte-rendu au format PDF" data-toggle="tooltip" role="button" value="Télécharger">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-6 mb-5 mb-xl-0">
                <div class="card bg-gradient-default shadow">
                    <div class="card-header bg-transparent">
                        <div class="row align-items-center">
                            <div class="col">
                                <h5 class="text-uppercase text-center text-light ls-1 mb-1">Répartition des protocoles des trames du fichier</h5>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <!-- Chart -->
                        <div class="chart">
                            <!-- Chart wrapper -->
                            <canvas id="doughnut-chart" height="600" width="600"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card shadow">
                    <div class="card-header bg-transparent">
                        <div class="row align-items-center">
                            <div class="col">
                                <h5 class="text-uppercase text-center text-muted ls-1 mb-1">Trames totales / non autorisées</h5>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <!-- Chart -->
                        <div class="chart">
                            <canvas id="ctx" width="800" height="450"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if(isset($totaltable)){?>
        <div class="row mt-5">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Connexions par serveur</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <tr class="thead-light">
                                <th scope="col">Sous réseaux</th>
                                <th scope="col">Nombre de requêtes autorisées</th>
                                <th scope="col">Nombre de requêtes interdites</th>
                            </tr>
                            <?php foreach ($totaltable as $table){ ?>
                            <tr>
                                <td scope="row"><?=$table['name'];?></td>
                                <td><?=$table['authorize'];?></td>
                                <td><?=$table['forbidden'];?></td>
                            </tr>
                            <?php }?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <!-- Footer -->
        <footer class="footer">
            <div class="row align-items-center justify-content-xl-between">
                <div class="col-xl-12">
                    <div class="copyright text-center text-xl-center text-muted">
                        &copy; 2019 Network Tracker
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- Argon Scripts -->
<!-- Core -->
<script src="./assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="./assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- Optional JS -->
<script src="./assets/vendor/chart.js/dist/Chart.min.js"></script>
<script src="./assets/vendor/chart.js/dist/Chart.extension.js"></script>
<!-- Argon JS -->
<script src="./assets/js/argon.js?v=1.0.0"></script>
<script>

    //Chart pour le polar

    let nbtotaltrame = <?php if (count($serveurs) != 0){ echo $nbtotaltrame * count($serveurs);} else { echo $nbtotaltrame;} ?>;
    let chartColors = window.chartColors;

    new Chart(ctx, {
        type: 'polarArea',
        data: {
            datasets: [{
                data: [nbtotaltrame, <?= $toto; ?>],
            }],
                backgroundColor: [
                    '#ff0000',
                    '#0000ff',
                ],
                labels: [
                    'Nombre total de trames',
                    'Trames non autorisées'
                ],
            },
        options: {
            responsive: true,
            legend: {
                position: 'right',
            },
            scale: {
                ticks: {
                    beginAtZero: true
                },
                reverse: false
            },
            animation: {
                animateRotate: false,
                animateScale: true
            }
        }
        });

    // Chart pour le donut

    new Chart(document.getElementById("doughnut-chart"), {
        type: 'pie',
        data: {
            labels: [<?php foreach ($protocols as $key => $value){ echo '"' . $key . '",'; }?>],
            datasets: [
                {
                    label: "Requête (nombre)",
                    backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#30a317","#c24dbd","#8fd574","#da3c66","#d3c15e","#548399","#93501b","#123abc","#123abc","#f57770","#500de2","#68f988","#daf7dc","#63452d","#afaff9","#51e8e8","#cc45bc"],
                    data: [<?php foreach ($protocols as $key => $value){ echo '"' . $value . '",'; }?>]
                }
            ]
        }
    });

    $('#myCollapsible').collapse({
        toggle: false
    });


</script>
</body>

<?php } else
    header('Location: login.php');




