<?php

$filename = 'rapport.pdf';
$path = $_SERVER['DOCUMENT_ROOT']. '/Network-Tracker/';
$file = $path . "/" . $filename;

$mailto = 'bat76640@gmail.com';
$from_mail = "admin@admin.com"; //Expediteur
$from_name = "Network-Tracker"; //Votre nom, ou nom du site
$reply_to = "bat76640@gmail.com"; //Adresse de réponse

$subject = 'Rapport d\'analyse';
$message = 'Voici votre rapport d\'analyse';

$content = file_get_contents($file);
$content = chunk_split(base64_encode($content));

// a random hash will be necessary to send mixed content
$separator = md5(time());

// carriage return type (RFC)
$eol = "\r\n";

// main header (multipart mandatory)
$headers = "From: $from_mail" . $eol;
$headers .= "Reply-to: $from_mail". $eol;
$headers .= "X-Priority: 1 \n";
$headers .= "MIME-Version: 1.0" . $eol;
$headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
$headers .= "Content-Transfer-Encoding: 7bit" . $eol;
$headers .= "This is a MIME encoded message." . $eol;

// message
$body = "--" . $separator . $eol;
$body .= "Content-Type: text/plain; charset=\"iso-8859-1\"" . $eol;
$body .= "Content-Transfer-Encoding: 8bit" . $eol;
$body .= $message . $eol;

// Pièce jointe
$body .= "--" . $separator . $eol;
$body .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"" . $eol;
$body .= "Content-Transfer-Encoding: base64" . $eol;
$body .= "Content-Disposition: attachment" . $eol;
$body .= $content . $eol;
$body .= "--" . $separator . "--";

if (mail($mailto, $subject, $body, $headers)) {
    unlink('rapport.pdf');
    header('location: index.php');
} else {
    echo "mail send ... ERROR!";
    print_r( error_get_last() );
}

