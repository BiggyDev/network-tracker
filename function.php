<?php

function afficherinfotrame($trame){
    echo 'Trame numéro: ' . $trame['_source']['layers']['frame']['frame.number'];
    echo '<br>';
    echo 'Protocol: ' . $trame['_source']['layers']['frame']['frame.protocols'];
    echo '<br>';
    if(isset($trame['_source']['layers']['ip']['ip.src'])){
        echo 'ipsource: ' . $trame['_source']['layers']['ip']['ip.src'];
        echo '<br>';
    }
    if(isset($trame['_source']['layers']['ip']['ip.dst'])){
        echo 'ipdest: ' . $trame['_source']['layers']['ip']['ip.dst'];
        echo '<br>';
    }

    echo '<br>';
}

function countnbtrametotal($fichier){
    return count($fichier);
}

function countprotocol($data,$protocols){

    $protocol = explode(':', $data['_source']['layers']['frame']['frame.protocols']); // découper frame protocol
    $protocol = end($protocol); //prendre dernière valeur du tableau (protocol)

    if (isset($protocols[$protocol])) { $protocols[$protocol] ++ ; } // incrémentation protocol
    else { $protocols[$protocol] = 1 ; } // sinon initialiser le nouveau protocol
    return $protocols;
}

// use strrevpos function in case your php version does not include it
function strrevpos($instr, $needle)
{
    $rev_pos = strpos (strrev($instr), strrev($needle));
    if ($rev_pos===false) return false;
    else return strlen($instr) - $rev_pos - strlen($needle);
}

function after_last ($toto, $inthat)
{
    if (!is_bool(strrevpos($inthat, $toto)))
        return substr($inthat, strrevpos($inthat, $toto)+strlen($toto));
}

function cidr_match($ip, $range)
{
    list ($subnet, $bits) = explode('/', $range);
    if ($bits === null) {
        $bits = 32;
    }
    $ip = ip2long($ip);
    $subnet = ip2long($subnet);
    $mask = -1 << (32 - $bits);
    $subnet &= $mask; # nb: in case the supplied subnet wasn't correctly aligned
    return ($ip & $mask) == $subnet;
}