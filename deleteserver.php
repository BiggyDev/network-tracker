<?php
include('inc/pdo.php');
include('inc/functions.php');

$nomTable = "nt_server";

if(isset($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
    delete($nomTable,$id);
    header('Location: index.php');
}