<?php

// Vérifie si l'adresse IP est correcte
function verificationIpField($error,$field,$textfield) {
    if(isset($field)){
        if (!filter_var($field, FILTER_VALIDATE_IP)) {
            $error[$textfield] = 'L\'adresse IP ' . $field . ' est considérée comme invalide.';
        }
    }
    return $error;
}
// Fin fonction

// Vérifie si l'adresse MAC est correcte
function verificationMacAddrField($error,$field,$textfield) {
    if(isset($field)){
        if (!filter_var($field, FILTER_VALIDATE_MAC)) {
            $error[$textfield] = 'L\'adresse MAC ' . $field . ' est considérée comme invalide.';
        }
    }
    return $error;
}
// Fin fonction


// Vérifie si les champs sont remplis et de la bonne longueur
function verificationfullField($error,$field, $textfield, $min, $max) {
  if(isset($field)){
      if(strlen($field) < $min ) {
          $error[$textfield] = 'Champ trop court. (Minimum ' . $min . ' caractères)';
      } elseif(strlen($field) > $max) {
          $error[$textfield] = 'Champ trop long. (Maximum ' . $max . ' caractères)';
      } else {
        $error[$textfield] = 'Veuillez renseigner ce champ';
      }
  }
  return $error;
}
// Fin fonction

//Vérifie si l'utilisateur est connecté
function isLogged () {
    if(!empty($_SESSION['nt_user']) &&
        !empty($_SESSION['nt_user']['id']) &&
        !empty($_SESSION['nt_user']['email']) &&
        !empty($_SESSION['nt_user']['role']) &&
        !empty($_SESSION['nt_user']['ip'])) {
        if($_SESSION['nt_user']['ip'] == $_SERVER['REMOTE_ADDR']) {
            return true;
        }
    } else {
      return false;
    }
}
//Fin fonction

//Vérifie si l'utilisateur est un administrateur
function isAdmin () {
    if(isLogged()){
        if($_SESSION['nt_user']['role'] == 'admin'){
            return true;
        }
    }
    return false;
}
//Fin fonction

//Supprime un utilisateur ou un vaccin de la BDD
function delete($nomTable,$id){
    global $pdo;

    $sql="DELETE FROM $nomTable WHERE id=:id";
    $query = $pdo ->prepare($sql);
    $query -> bindValue(':id',$id,PDO::PARAM_INT);
    $query -> execute();
}
//Fin fonction
